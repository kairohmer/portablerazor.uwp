﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace PortableRazor.UWP
{
    internal static class Threading
    {
        private static Windows.UI.Core.CoreDispatcher MainThreadDispatcher
        {
            get
            {
                if (_MainThreadDispatcher == null)
                {
                    if (Windows.ApplicationModel.Core.CoreApplication.Views.Count == 1 &&
                        Windows.ApplicationModel.Core.CoreApplication.Views[0].CoreWindow == null)
                        return null;

                    try
                    {
                        _MainThreadDispatcher = Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher;

                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
                return _MainThreadDispatcher;
            }
        }
        private static Windows.UI.Core.CoreDispatcher _MainThreadDispatcher;

        public static bool IsRunningOnGuiThread
        {
            get
            {
                if (MainThreadDispatcher == null) return false;
                return MainThreadDispatcher.HasThreadAccess;
            }
        }
        
        /// <summary>
        /// Send an action to perform on the main (UI) thread.
        /// </summary>
        /// <param name="a">An action to run on the UI-Thread, e.g. changing properties of UI elements.</param>
        public static async Task DispatchToGuiThread(Action a)
        {
            try
            {
                // run directly if we are on UI thread
                if (IsRunningOnGuiThread)
                {
                    a();
                    return;
                }
                // dispatch if not
                await MainThreadDispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => a()).AsTask();
            }
            catch (OperationCanceledException)
            {
                // this fine if the process was stopped from the outside
            }
            catch (Exception ex)
            {
                throw new Exception("[DispatchToGuiThread] Dispatch Failed: " + ex.Message);
            }
        }

        
        public static int GetThreadId()
        {
            return Environment.CurrentManagedThreadId;
        }
    }

}
