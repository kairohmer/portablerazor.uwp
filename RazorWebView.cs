﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Controls;
using Windows.Web;
using PortableRazor;
using PortableRazor.Utilities;

namespace PortableRazor.UWP
{
    public class RazorWebView : UserControl, IHybridWebView
    {
        /// <summary>
        /// The context of this webview.
        /// It's the main API access point that offers pretty much everything (that is implemented).
        /// </summary>
        public PortableRazorContext RazorContext { get; private set; }

        /// <summary>
        /// Invoked from the java script code.
        /// This event is processed by the Context.
        /// </summary>
        public event EventHandler<string> NotifyFromJavascript;

        /// <summary>
        /// Invoked during naviation to load a new controller / action.
        /// This event is processed by the Context.
        /// </summary>
        public event EventHandler<RoutingEventArgs> RoutingRequest;

        /// <summary>
        /// Native webview controll.
        /// </summary>
        private readonly WebView _WebView;


        /// <summary>
        /// Log for this object.
        /// </summary>
        private readonly Logging _Log = new Logging("RazorWebView");

        /// <summary>
        /// Creates a new RazorWebView.
        /// Usually this is done using xaml.
        /// </summary>
        public RazorWebView()
        {
            // create a context first
            RazorContext = new PortableRazorContext(this);

            // create native elements
            _WebView = new WebView();
            Content = _WebView;

            // we can intercept all navigations to URIs starting with "hybrid:"
            _WebView.UnsupportedUriSchemeIdentified += (sender, args) =>
            {
                try
                {
                    RoutingEventArgs routingArgs = new RoutingEventArgs(args.Uri);
                    RoutingRequest?.Invoke(this, routingArgs);
                    args.Handled = routingArgs.Handled; 
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
                
            };

            _WebView.NavigationStarting += (sender, args) =>
            {
                // catch links that go out of the app here
            };

            _WebView.NavigationCompleted += (sender, args) =>
            {
            };

            _WebView.ScriptNotify += WebViewOnScriptNotify;
        }

        private void WebViewOnScriptNotify(object sender, NotifyEventArgs notifyEventArgs)
        {
            var args = notifyEventArgs.Value;

            Task.Run(() =>
            {
                try
                {

                    NotifyFromJavascript?.Invoke(this, args);
                }
                catch (Exception ex)
                {
                    _Log.Error("Failed to handle call from Javascript.", ex);
                }
            });
        }



        public string BasePath => System.IO.Path.Combine(BaseResourceManager.ContentPath, "www");

        public async void LoadHtmlString(string html)
        {
            try
            { 
                await Threading.DispatchToGuiThread(() =>
                {
                    Uri url = _WebView.BuildLocalStreamUri("html", "none");
                    HtmlStreamResolver myResolver = new HtmlStreamResolver(url, html, BasePath);
                    _WebView.NavigateToLocalStreamUri(url, myResolver);
                });

            }
            catch (Exception ex)
            {
                _Log.Error("Failed to load html from string.", ex);
            }
        }

        public async Task<string> EvaluateJavascriptAsync(string script)
        {
            string result = "";
            SemaphoreSlim waitHandle = new SemaphoreSlim(0, 1);

            // make sure the script is evaluated on the main thread
            // if not at least windows will throw an exception..
            await Threading.DispatchToGuiThread(() =>
            {
                try
                {
                    var task = _WebView.InvokeScriptAsync("eval", new string[] {script});
                    task.Completed = (info, status) =>
                    {
                        if(status == AsyncStatus.Completed)
                            result = info.GetResults();

                        waitHandle.Release();
                    };
                }
                catch (Exception ex)
                {
                    _Log.Error("Evaluating Javascript failed", ex);
                    //Debug.WriteLine($"Eval exception: {ex.Message}");
                    waitHandle.Release();
                }
            });

            await waitHandle.WaitAsync();
            return result;
        }

        /// <summary>
        /// Allows to navigate to an HTML page given as string and resolve the content relative to a BasePath.
        /// So, the HTML page can have relative paths for images, css and java-scripts.
        /// Usage:
        ///     Uri url = _WebView.BuildLocalStreamUri("html", "none"); // it does not matter what you pass here but make sure it doesn't collide with other stuff
        ///     HtmlStreamResolver myResolver = new HtmlStreamResolver(url, html, BasePath);
        ///     _WebView.NavigateToLocalStreamUri(url, myResolver);
        /// </summary>
        private class HtmlStreamResolver : IUriToStreamResolver
        {
            private Uri _Uri;
            private String _Html;
            private String _BasePath;

            /// <summary>
            /// Creates an instance of HtmlStreamResolver 
            /// </summary>
            /// <param name="uri">a LocalStreamUri build by the WebView, parameter don't matter (but can't be null, unfortunately)</param>
            /// <param name="html">Your HTML page to navigate to, given as string</param>
            /// <param name="basePath">The path to your content (probably containing the previously extracted embedded content</param>
            public HtmlStreamResolver(Uri uri, String html, String basePath)
            {
                _Uri = uri;
                _Html = html;
                _BasePath = basePath;
            }

            public IAsyncOperation<IInputStream> UriToStreamAsync(Uri uri)
            {
                if (uri == null)
                    throw new Exception();

                if (uri == _Uri)
                    return GetHtml().AsAsyncOperation();

                // Because of the signature of the this method, it can't use await, so we  
                // call into a separate helper method that can use the C# await pattern. 
                return GetContent(uri.AbsolutePath).AsAsyncOperation();
            }

            private async Task<IInputStream> GetHtml()
            {
                try
                {
                    var stream = new InMemoryRandomAccessStream();
                    using (var dataWriter = new DataWriter(stream))
                    {
                        dataWriter.WriteString(_Html);
                        await dataWriter.StoreAsync();
                        await dataWriter.FlushAsync();
                        dataWriter.DetachStream(); // does not close the stream
                    }

                    return stream.GetInputStreamAt(0);
                }
                catch (Exception)
                {
                    throw new Exception("Invalid usage");
                }
            }

            private async Task<IInputStream> GetContent(string path)
            {
                try
                {
                    // not sure if the path always starts with a slash but combining the filePath manually is safe
                    if (path.StartsWith("/"))
                        path = path.Substring(1);

                    var filePath = Path.Combine(_BasePath, path).Replace("/", "\\");
                    var file = await StorageFile.GetFileFromPathAsync(filePath);
                    return await file.OpenAsync(FileAccessMode.Read);
                }
                catch (Exception)
                {
                    throw new Exception("Invalid path");
                }
            }
        }
    }
}
