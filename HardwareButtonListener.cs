﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.Xaml;

namespace PortableRazor.UWP
{


    public class HardwareButtonListener : IHardwareButtonListener
    {
        public event EventHandler Back;

        public HardwareButtonListener()
        {
            // this event is not catched in in most cases..
            // the event is stopped inside the WebView i suppose to prevent people from creating key logged browsers
            // the only way I can think of is javascript code to notify the application code
            // on a mobile with real hardware buttons this might be easier..
            
            //Window.Current.CoreWindow.KeyUp += CoreWindowOnKeyUp;
        }

        private void CoreWindowOnKeyUp(CoreWindow sender, KeyEventArgs args)
        {
            if (args.VirtualKey == VirtualKey.Escape)
                Back?.Invoke(this, EventArgs.Empty);
        }
    }
}
