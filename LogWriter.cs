using PortableRazor.Utilities;

namespace PortableRazor.UWP
{
    internal class LogWriter : ILogWriter
    {
        public void Print(LogLevel level, string message)
        {
            System.Diagnostics.Debug.WriteLine($"[{level.ToString()}] {message}");
        }
    }
}